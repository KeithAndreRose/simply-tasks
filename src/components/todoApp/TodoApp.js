import React, { useState, useRef, useEffect } from "react";
import AddIcon from "../../assets/icons/AddIcon";
import { TodoList } from ".";
import uuidv4 from "uuid/v4";
import "./styles/TodoApp.scss";

const LOCAL_STORAGE_KEY = "simplyTask.todos";

export default function TodoApp() {
  const [todos, setTodos] = useState([]);
  const todoInputRef = useRef();

  useEffect(() => {
    const storedTodos = localStorage.getItem(LOCAL_STORAGE_KEY);
    if (storedTodos) setTodos(JSON.parse(storedTodos));
  }, []);

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(todos));
  }, [todos]);

  function toggleTodo(id) {
    const todoList = [...todos];
    const targetTodo = todoList.find(i => i.id === id);
    targetTodo.complete = !targetTodo.complete;
    setTodos(todoList);
  }

  function createTodo(evt) {
    const task = todoInputRef.current.value;
    if (task === "") return;
    const newTodo = {
      id: uuidv4(),
      task,
      complete: false
    };
    setTodos(prevTodos => {
      return [...prevTodos, newTodo];
    });
    todoInputRef.current.value = null;
  }

  function deleteTodo(todo) {
    console.log('TARGET',todo)
    const todoList = todos.filter(i => i !== todo);
    setTodos(todoList);
  }

  function clearCompletedTodos() {
    const todoList = todos.filter(i => !i.complete);
    setTodos(todoList);
  }

  return (
    <div className="todo-app">
      <div className="todo-input-container">
        <input
          className="todo-input"
          ref={todoInputRef}
          placeholder="Create task..."
          onKeyDown={({ key }) => (key === "Enter" ? createTodo() : 0)}
          type="text"
        />
        <button className="todo-add-button" onClick={createTodo}>
          <AddIcon />
        </button>
      </div>
      <div className="todo-incomplete-count">
        {todos.filter(i => !i.complete).length} left to do
      </div>
      <div className="todo-controls">
        <button
          className="todo-button todo-remove-completed-button"
          onClick={clearCompletedTodos}
        >
          Clear Completed Todos
        </button>
      </div>
      <TodoList todos={todos} toggleTodo={toggleTodo} deleteTodo={deleteTodo} />
    </div>
  );
}
