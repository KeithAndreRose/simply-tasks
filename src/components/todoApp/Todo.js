import React, { useState, useRef, useEffect } from "react";
import "./styles/Todo.scss";
import CheckIcon from "../../assets/icons/CheckIcon";

export default function Todo({ todo, toggleTodo, deleteTodo, editTodo }) {
  const [editing, setEditing] = useState(false);
  const todoRef = useRef(null);
  function toggleComplete() {
    toggleTodo(todo.id);
    console.log("togo");
  }
  function toggleEditing() {
    setEditing(prev => !prev);
    if (!todoRef || !todoRef.current) return;
    todoRef.current.focus();
  }

  return (
    <div className="todo">
      <input
        className="todo-status"
        type="checkbox"
        checked={todo.complete}
        onChange={toggleComplete}
        ref={todoRef}
      />
      <span className="todo-check">
        <CheckIcon/>
      </span>
      { !editing ? (
        <span className="todo-task" onClick={toggleComplete}>
          {todo.task}
        </span>
      ) : (
        <input
          className="todo-task"
          onChange={() => {}}
          disabled={!editing}
          value={todo.task}
        />
      )}
      <span
        className="todo-edit"
        onClick={toggleEditing}
        role="img"
        aria-label="edit"
      >
        📝
      </span>
      <span
        className="todo-delete"
        onClick={() => deleteTodo(todo)}
        role="img"
        aria-label="delete"
      >
        ❌
      </span>
    </div>
  );
}
