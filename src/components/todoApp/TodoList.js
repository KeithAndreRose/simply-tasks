import React from "react";
import Todo from "./Todo";
import "./styles/TodoList.scss";

export default function TodoList({ todos, toggleTodo, deleteTodo }) {
  return (
    <div className="todo-list">
      <span className="todo-list-label">Tasks:</span>
      {todos.reverse().map(todo => {
        return (
          <Todo
            key={todo.id}
            todo={todo}
            toggleTodo={toggleTodo}
            deleteTodo={deleteTodo}
          />
        );
      })}
    </div>
  );
}
