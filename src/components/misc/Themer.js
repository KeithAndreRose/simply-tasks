import React, { useEffect, useState, useCallback } from "react";
import { Helmet } from "react-helmet";
import "./styles/Themer.scss";
const themePack = {
  light: {
    name: "light",
    hue1: "#BDC3C7",
    hue2: "#c6cccf",
    hue3: "#d0d4d7",
    hue4: "#d9dddf",
    hue5: "#e3e5e7",
    hue6: "#eceeef",
    hue7: "#f6f6f7",
    text: "#000"
  },
  dark: {
    name: "dark",
    hue1: "#0a0a0a",
    hue2: "#111111",
    hue3: "#181818",
    hue4: "#1f1f1f",
    hue5: "#252525",
    hue6: "#2c2c2c",
    hue7: "#333333",
    text: "#fff"
  },
  red: {
    name: "red",
    hue1: "#FF5F5F",
    hue2: "#ff6a6a",
    hue3: "#ff7474",
    hue4: "#ff7f7f",
    hue5: "#ff8a8a",
    hue6: "#ff9494",
    hue7: "#ff9f9f",
    text: "#fff"
  },
  orange: {
    name: "orange",
    hue1: "#FFB851",
    hue2: "#ffbd5d",
    hue3: "#ffc168",
    hue4: "#ffc674",
    hue5: "#ffcb7f",
    hue6: "#ffd08b",
    hue7: "#ffd497",
    text: "#2f2f2f"
  },
  cyan: {
    name: "cyan",
    hue1: "#00FFFF",
    hue2: "#44ffff",
    hue3: "#66ffff",
    hue4: "#88ffff",
    hue5: "#aaffff",
    hue6: "#ccffff",
    hue7: "#eeffff",
    text: "#333"
  },

};

const LOCAL_STORAGE_KEY = "simplyTask.theme";
// Applies the time to a style template
export default function Themer() {
  const [theme, setTheme] = useState(null);
  const applyTheme = useCallback(theme => {
    setTheme(() => {
      localStorage.setItem(LOCAL_STORAGE_KEY, theme);
      return themePack[theme]
    })
  }, [])
  
  useEffect(() => {
    const theme = localStorage.getItem(LOCAL_STORAGE_KEY);
    return setTheme(() => themePack[theme])
  }, []);

  return (
    <div className="themer">
      {Object.entries(themePack)
        .map(i => i[1])
        .map(theme => (
          <span
            className="theme-select"
            key={theme.name}
            style={{ background: theme.hue1 }}
            onClick={() => applyTheme(theme.name)}
          ></span>
        ))}
      {/* <span className="theme-select"></span> */}
      {/* <span className="theme-select"></span> */}

      <Helmet>
        <style>
          {theme
            ? `:root{
              --hue1: ${theme.hue1};
              --hue2: ${theme.hue2};
              --hue3: ${theme.hue3};
              --hue4: ${theme.hue4};
              --hue5: ${theme.hue5};
              --hue6: ${theme.hue6};
              --hue7: ${theme.hue7};
              --text: ${theme.text} 
              }`
            : null}
        </style>
      </Helmet>
    </div>
  );
}
