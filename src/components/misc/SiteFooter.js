import React from 'react'
import './styles/SiteFooter.scss'

export default function SiteFooter() {
  return (
    <footer className="site-footer">
      <div className="site-footer-inner">Simply Task ®</div>
      <div className="site-footer-extra">
        <span>Created by Keith Rose</span>
        <span> &copy; 2019 Firebrand Developments LLC. All Rights Reserved.</span>
        <span> Unauthorized duplication, in whole or in part, is strictly prohibited.</span>
        <div className="extra-links">
          <a href="https://keithandrerose.com">My Portfolio <span role="img" aria-label="portfolio">💼</span></a>
        </div>
      </div>
      <div className="site-footer-background"></div>
    </footer>
  )
}
