import React from "react";
import "./styles/HeaderNavigation.scss";
import MiniTimerApp from "../timerApp/MiniTimerApp";

export default function HeaderNavigation() {
  return (
    <header>
      <nav>
        <div className="logo-wrapper">
          <a href="/">Simply Task</a>
        </div>
        <div className="nav-items">
          <MiniTimerApp/>
        </div>
      </nav>
    </header>
  );
}
