import React, { useState, useCallback } from "react";
import BreakInterval from "./BreakInterval";
import SessionLength from "./SessionLength";
import TimerDisplay from "./TimerDisplay";
import "./styles/Timer.scss";

export default function Timer() {
  const [breakLength, setBreakLength] = useState(5);
  const [sessionLength, setSessionLength] = useState(25);
  const [timerMinute, setTimerMinute] = useState(25);
  const [isPlaying, setIsPlaying] = useState(false);

  const onIncreaseBreakInterval = useCallback(() => {
    setBreakLength(length => length + 1);
  }, [setBreakLength])

  const onDecreaseBreakInterval = useCallback(() => {
    setBreakLength(length => length - 1);
  }, [setBreakLength])

  const onIncreaseSessionInterval = useCallback(() => {
    setSessionLength(length => {
      setTimerMinute(length + 1);
      return length + 1;
    });
  }, [setSessionLength,setTimerMinute])

  const onDecreaseSessionInterval = useCallback(() => {
    setSessionLength(length => {
      setTimerMinute(length - 1);
      return length - 1;
    });
  }, [setSessionLength,setTimerMinute])

  const onUpdateTimerMinute = useCallback(() => {
    let minute = 0;
    setTimerMinute(prev => {
      minute = prev - 1
      return minute
    });
    return minute
  }, [setTimerMinute])

  const onToggleInterval = useCallback(isSession => {
    setTimerMinute(isSession ? sessionLength - 1 : breakLength - 1);
  }, [setTimerMinute, sessionLength, breakLength])

  const onResetTimer = useCallback(() => {
    setTimerMinute(sessionLength);
  }, [setTimerMinute, sessionLength])

  const onTimerStartStop = useCallback(playStatus=> {
    setIsPlaying(playStatus)
  },[setIsPlaying])

  return (
    <main className="timer">
      <TimerDisplay
        timerMinute={timerMinute}
        breakLength={breakLength}
        updateTimerMinute={onUpdateTimerMinute}
        toggleInterval={onToggleInterval}
        resetTimer={onResetTimer}
        isPlaying={isPlaying}
        timerStartStop={onTimerStartStop}
      />
      <div className="timer-controls">
        <BreakInterval
          breakInterval={breakLength}
          onIncreaseBreakInterval={onIncreaseBreakInterval}
          onDecreaseBreakInterval={onDecreaseBreakInterval}
          isPlaying={isPlaying}
        />
        <SessionLength
          sessionLength={sessionLength}
          onIncreaseSessionInterval={onIncreaseSessionInterval}
          onDecreaseSessionInterval={onDecreaseSessionInterval}
          isPlaying={isPlaying}
        />
      </div>
    </main>
  );
}
