import React, { useState } from "react";
import { Helmet } from "react-helmet";

import PlayIcon from "../../assets/icons/PlayIcon";
import PauseIcon from "../../assets/icons/PauseIcon";
import ResetIcon from "../../assets/icons/ResetIcon";
import "./styles/MiniTimerDisplay.scss";

export default function MiniTimerDisplay({
  timerMinute,
  breakLength,
  updateTimerMinute,
  toggleInterval,
  resetTimer,
  isPlaying,
  timerStartStop
}) {
  const [isSession, setIsSession] = useState(true);
  const [timerSecond, setTimerSecond] = useState(0);
  const [intervalId, setIntervalId] = useState(0);

  function toggleTimer(){
    isPlaying 
    ? stopTimer() 
    : playTimer()
  }

  function playTimer() {
    let intervalId = setInterval(decreaseTimer, 1000);
    setIntervalId(intervalId);
    timerStartStop(true);
  }

  function stopTimer() {
    clearInterval(intervalId);
    timerStartStop(false);
  }

  function resetClock() {
    stopTimer();
    resetTimer();
    setTimerSecond(0);
    setIsSession(true);
  }

  function decreaseTimer() {
    setTimerSecond(previousSecond => {
      if (!previousSecond) {
        let minute = updateTimerMinute();
        if (minute === -1) {
          setIsSession(prev => {
            let audioCue = document.getElementById("sessionAlert");
            if (audioCue) audioCue.play();
            toggleInterval(!prev);
            return !prev;
          });
        }
        return 59;
      } else {
        return previousSecond - 1;
      }
    });
  }

  return (
    <div className="mini-timer-display">
      <Helmet>
        <title>
          {" "}
          Simply Task |{" "}
          {` ${isSession ? "[Session]" : "-- Break --"} |
          ${timerMinute}:${
            timerSecond
              ? timerSecond < 10
                ? "0" + timerSecond
                : timerSecond
              : "00"
          }`}
        </title>
      </Helmet>

      <div className="mini-timer-watch" onClick={toggleTimer}>
        <h4 className="mini-timer-session">
          {isSession ? "[Session]" : "-- Break --"}
        </h4>
        <div className="mini-timer-time">
          <span>{timerMinute}</span>
          <span>:</span>
          <span>
            {timerSecond
              ? timerSecond < 10
                ? "0" + timerSecond
                : timerSecond
              : "00"}
          </span>
        </div>
      </div>
      <div className="mini-timer-controls">
        <button
          className="mini-timer-control-button play"
          onClick={playTimer}
          disabled={isPlaying}
        >
          <PlayIcon />
        </button>
        <button
          className="mini-timer-control-button pause"
          onClick={stopTimer}
          disabled={!isPlaying}
        >
          <PauseIcon />
        </button>
        <button className="mini-timer-control-button reset" onClick={resetClock}>
          <ResetIcon />
        </button>
      </div>
    </div>
  );
}
