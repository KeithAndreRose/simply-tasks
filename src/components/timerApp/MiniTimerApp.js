import React, { useState, useCallback } from "react";
import BreakInterval from "./BreakInterval";
import SessionLength from "./SessionLength";
import MiniTimerDisplay from "./MiniTimerDisplay";
import "./styles/MiniTimer.scss";

export default function MinTimerApp() {
  const [breakLength] = useState(5);
  const [sessionLength] = useState(25);
  const [timerMinute, setTimerMinute] = useState(25);
  const [isPlaying, setIsPlaying] = useState(false);

  const onUpdateTimerMinute = useCallback(() => {
    let minute = 0;
    setTimerMinute(prev => {
      minute = prev - 1
      return minute
    });
    return minute
  }, [setTimerMinute])

  const onToggleInterval = useCallback(isSession => {
    setTimerMinute(isSession ? sessionLength - 1 : breakLength - 1);
  }, [setTimerMinute, sessionLength, breakLength])

  const onResetTimer = useCallback(() => {
    setTimerMinute(sessionLength);
  }, [setTimerMinute, sessionLength])

  const onTimerStartStop = useCallback(playStatus=> {
    setIsPlaying(playStatus)
  },[setIsPlaying])

  return (
    <main className="mini-timer">
      <MiniTimerDisplay
        timerMinute={timerMinute}
        breakLength={breakLength}
        updateTimerMinute={onUpdateTimerMinute}
        toggleInterval={onToggleInterval}
        resetTimer={onResetTimer}
        isPlaying={isPlaying}
        timerStartStop={onTimerStartStop}
      />
      {/* <div className="timer-controls">
        <BreakInterval
          breakInterval={breakLength}
          onIncreaseBreakInterval={onIncreaseBreakInterval}
          onDecreaseBreakInterval={onDecreaseBreakInterval}
          isPlaying={isPlaying}
        />
        <SessionLength
          sessionLength={sessionLength}
          onIncreaseSessionInterval={onIncreaseSessionInterval}
          onDecreaseSessionInterval={onDecreaseSessionInterval}
          isPlaying={isPlaying}
        />
      </div> */}
    </main>
  );
}
