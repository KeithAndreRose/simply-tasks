import React from "react";
import ArrowIcon from '../../assets/icons/ArrowIcon'

export default function BreakInterval({
  breakInterval,
  onIncreaseBreakInterval,
  onDecreaseBreakInterval
}) {
  function decreaseLength() {
    if (breakInterval === 1) return;
    else onDecreaseBreakInterval();
  }

  function increaseLength() {
    if (breakInterval === 60) return;
    else onIncreaseBreakInterval();
  }

  return (
    <section className="breakInterval">
      <h4>Break Interval</h4>
      <div className="controls-container">
        <button className="timer-button incrementDown" onClick={decreaseLength}>
          <ArrowIcon/>
        </button>
        <p>{breakInterval}</p>
        <button className="timer-button incrementUp" onClick={increaseLength}>
          <ArrowIcon/>
        </button>
      </div>
    </section>
  );
}
