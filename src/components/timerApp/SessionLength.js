import React from "react";
import ArrowIcon from '../../assets/icons/ArrowIcon'

export default function SessionLength({
  sessionLength,
  onIncreaseSessionInterval,
  onDecreaseSessionInterval
}) {
  function decreaseLength() {
    if (sessionLength === 1) return;
    else onDecreaseSessionInterval();
  }

  function increaseLength() {
    if (sessionLength === 60) return;
    else onIncreaseSessionInterval();
  }
  return (
    <div className="sessionLength">
      <h4>Session Length</h4>
      <div className="controls-container">
        <button className="timer-button incrementDown" onClick={decreaseLength}>
          <ArrowIcon/>
        </button>
        <p>{sessionLength}</p>
        <button className="timer-button incrementUp" onClick={increaseLength}>
          <ArrowIcon/>
        </button>
      </div>
    </div>
  );
}
