import React from "react";
import HeaderNavigation from './components/layout/HeaderNavigation'
import SiteFooter from './components/misc/SiteFooter'
import TodoApp from './components/todoApp/TodoApp'
import "./App.css";

function App() {
  return (
  <>
    <HeaderNavigation/>
    <TodoApp/>
    <SiteFooter/>
  </>
  )
}

export default App;
