import React from "react";

export default function PauseIcon() {
  return (
    <svg
      height="16"
      width="16"
      viewBox="0 0 847 847"
    >
      <path d="M62 777l0 -708c0,-31 25,-56 56,-56l191 0c31,0 57,25 57,56l0 708c0,32 -26,57 -57,57l-191 0c-31,0 -56,-25 -56,-57zm419 0l0 -708c0,-31 25,-56 56,-56l191 0c31,0 57,25 57,56l0 708c0,32 -26,57 -57,57l-191 0c-31,0 -56,-25 -56,-57z"></path>
    </svg>
  );
}
