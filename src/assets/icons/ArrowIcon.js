import React from "react";

export default function ArrowIcon() {
  return (
    <svg
      height="16"
      width="16"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 60 60"
      x="0px"
      y="0px"
    >
      <path d="M0,30A30,30,0,1,0,30,0,30,30,0,0,0,0,30Zm54.3,0A24.3,24.3,0,1,1,30,5.7,24.33,24.33,0,0,1,54.3,30Z"></path>
      <path d="M14.61,34l3.93,3.93L29.89,26.54a.18.18,0,0,1,.26,0L41.46,37.86l3.93-3.93L34.07,22.62a5.73,5.73,0,0,0-8.11,0Z"></path>
    </svg>
  );
}
